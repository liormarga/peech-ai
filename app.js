var express = require('express');
var app = express();
var videoDeiscr = require("./videoDeiscriptor")


app.get('/', function (req, res) {
    
    var response = [];
    var videoDeiscriptor =videoDeiscr;
    
    if (Object.keys(req.query).length === 0) {
        response = videoDeiscriptor;
    } 

    var filename = req.query.filename;
    if (req.query.filename != undefined) filename = filename.toLowerCase();

    videoDeiscriptor.map(function (proj) {
        if (proj.filename.toLowerCase().includes(filename)) {
            response.push(proj);
        } 
        
       });
    
    if (response.length > 0 && req.query.authorid != undefined){
        videoDeiscriptor = response;
        response =[];
    }


    videoDeiscriptor.map(function (proj) {
        if (proj.authorId.includes(req.query.authorid)) {
            response.push(proj);
        } 
    });

    if (response.length > 0 && (req.query.durationstart != undefined|| req.query.durationstop != undefined)){
        videoDeiscriptor = response;
        response =[];
    }

    videoDeiscriptor.map(function (proj) {
        if (proj.duration >= req.query.durationstart && proj.duration <= req.query.durationstop) {
            response.push(proj);
        } 
        if (proj.duration >= req.query.durationstart && req.query.durationstop == undefined) {
            response.push(proj);
        } 
        if (proj.duration <= req.query.durationstop && req.query.durationstart == undefined) {
            response.push(proj);
        } 
    });

    if (response.length > 0 && req.query.tags != undefined){
        videoDeiscriptor = response;
        response =[];
    }

    videoDeiscriptor.map(function (proj) {
        var stop = false;
        proj.tags.map(function(tag){
            if (tag.includes(req.query.tags)&& !stop) {
                response.push(proj);
                stop = true;
            }
        })
        
    });


    res.send(response);
  
});

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});

module.exports = app; //for testing