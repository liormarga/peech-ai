process.env.NODE_ENV = 'test';

let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
chai.use(chaiHttp);

describe('/GET videos', () => {
    it('it should GET ALL videos', (done) => {
        chai.request(server)
        .get('/')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(12);
        done();
      });
    });
});

describe('/GET videos by name', () => {
    it('it should GET ALL videos start with p', (done) => {
        chai.request(server)
        .get('/?filename=p')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(6);
        done();
      });
    });
});

describe('/GET videos by name', () => {
    it('it should GET ALL videos exact match with Productdemo', (done) => {
        chai.request(server)
        .get('/?filename=Product demo')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(6);
        done();
      });
    });
});

describe('/GET videos by name', () => {
    it('it should GET ALL user start with user1', (done) => {
        chai.request(server)
        .get('/?authorid=user1')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(4);
        done();
      });
    });
});

describe('/GET videos by duration stop', () => {
    it('it should GET ALL duration stop with 2000', (done) => {
        chai.request(server)
        .get('/?durationstop=2000')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(8);
        done();
      });
    });
});

describe('/GET videos by duration start', () => {
    it('it should GET ALL duration start with 300', (done) => {
        chai.request(server)
        .get('/?durationstart=300')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(9);
        done();
      });
    });
});


describe('/GET videos by duration start end', () => {
    it('it should GET ALL duration start with 300 and end with 2000', (done) => {
        chai.request(server)
        .get('/?durationstart=300&durationstop=2000')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(5);
        done();
      });
    });
});


describe('/GET videos by tags', () => {
    it('it should GET ALL tags contains test ', (done) => {
        chai.request(server)
        .get('/?tags=test')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(3);
        done();
      });
    });
});


describe('/GET videos by filename and tags', () => {
    it('it should GET ALL videos filename product demo and tags contains prod ', (done) => {
        chai.request(server)
        .get('/?filename=p&tags=pro')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(6);
        done();
      });
    });
});


describe('/GET videos by filename and tags', () => {
    it('it should GET ALL videos filename product demo and empty tags', (done) => {
        chai.request(server)
        .get('/?filename=p&tags=')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(6);
        done();
      });
    });
});

describe('/GET videos by all types', () => {
    it('it should GET ALL videos filename webinar demo and  and authourid user tags marketing', (done) => {
        chai.request(server)
        .get('/?filename=webinar&authorid=user&durationstart=3000&durationstop=4000&tags=marketing')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(2);
        done();
      });
    });
});

describe('/GET videos by all types', () => {
    it('it should GET ALL videos filename webinar demo and duration between 3000 and 4000  and authourid user1 tags marketing', (done) => {
        chai.request(server)
        .get('/?filename=webinar&authorid=user2&durationstart=3000&durationstop=4000&tags=marketing')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(1);
        done();
      });
    });
});

describe('/GET videos by all types with wrong range', () => {
    it('it should GET ALL videos filename webinar demo  and authourid user1 tags marketing ignore duration since range not valid', (done) => {
        chai.request(server)
        .get('/?filename=webinar&authorid=user&durationstart=3000&durationstop=0&tags=marketing')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(3);
        done();
      });
    });
});

describe('/GET videos by all types with wrong range', () => {
    it('it should GET ALL videos filename webinar demo  and authourid user1 tags marketing ignore duration since range not valid', (done) => {
        chai.request(server)
        .get('/?filename=webinar&authorid=user&durationstart=3000&durationstop=&tags=marketing')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(3);
        done();
      });
    });
});

describe('/GET videos by all types and check having filename', () => {
    it('it should GET ALL videos filename webinar demo and duration between 3000 and 4000  and authourid user2 tags marketing', (done) => {
        chai.request(server)
        .get('/?filename=webinar&authorid=user2&durationstart=3000&durationstop=4000&tags=marketing')
        .end((err, res) => {
            res.body.should.be.a('array');
            res.body.length.should.be.eql(1);
            res.body[0].should.have.property('filename').eql("Webinar");
        done();
      });
    });
});


